# Generated by Django 5.0.6 on 2024-05-31 09:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_alter_rating_unique_together_rating_created_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mediaupload',
            name='category',
            field=models.CharField(choices=[('wav2lip', 'Wav2Lip'), ('sadtalker', 'SadTalker')], default='wav2lip', max_length=50),
        ),
    ]

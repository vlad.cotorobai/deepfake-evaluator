# DeepFake Video Evaluator

## Overview
DeepFake Video Evaluator is a comprehensive platform that allows users to generate, evaluate, and manage audio-driven deepfake videos, facilitating the comparison of different generation mechanisms.

## Features
- Authentication
- Generating deepfake videos functionality
- Rating generated videos functionality
- Setting your videos as private or public functionality
- Deleting private videos functionality
- Item gallery for private videos
- Item gallery for public videos

## Technologies Used
- **Frontend:**
  - HTML
  - CSS
  - Tailwindcss

- **Backend:**
  - Python Django
  - SQLite (Database)

## Project Structure (main project components)
- `core/`: Contains the main application code, including views, models, and templates.
  - `views.py`: Defines several functions for handling views and actions.
  - `models.py`: Defines the data models.
  - `forms.py`: Defines several form classes.
  - `templates/`: Contains HTML templates for the app.
- `marketplace/`: Contains the Django project settings and configurations.
  - `settings.py`: Configuration for the Django project.
  - `urls.py`: URL patterns for the project.

## How to Run on your device:
1. **Clone the repository:**
   ```bash
   git clone https://gitlab.upt.ro/vlad.cotorobai/deepfake-evaluator.git
   cd deepfake-evaluator
2. **Create and activate a virtual environment:**
   ```bash
   python -m venv venv
   source venv/bin/activate  # For Linux/Mac
   .\venv\Scripts\activate  # For Windows
3. **Install the required dependencies in each folder of each mechanism (Wav2Lip, MakeItTalk, SadTalker):**
   ```bash
   cd <Mechanism-Folder-Name> 
   pip install -r requirements.txt
4. **Apply the database migrations:**
   ```bash
   python manage.py migrate
5. **Run the Django development server:**
   ```bash
   python manage.py runserver
6. **Access the application:**
   Open your web browser and go to [http://localhost:8000](http://localhost:8000)

## Additional Notes
- Ensure you have `Python 3.6` or a newer version, `pip` and `Django` installed on your machine.